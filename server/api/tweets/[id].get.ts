import { tweetTransformer } from '~/server/transformers/tweet';
import { getTweetById } from '~/server/db/tweets'

export default defineEventHandler(async (event) => {
    const { id } = event.context.params

    try {
        const tweet = await getTweetById(id, {
            include: {
                author: true,
                mediaFiles: true,
                replyTo: {
                    include: {
                        author: true
                    }
                },
                replies: {
                    include: {
                        mediaFiles: true,
                        author: true,
                        replyTo: {
                            include: {
                                author: true
                            }
                        }
                    }
                },
            },
        })
        
        if (!tweet) throw Error('Tweet Not Found')

        return {
            tweet: tweetTransformer(tweet),
        }
    } catch (error) {
        sendError(event, createError({
            statusCode: 404,
            statusMessage: (error as Error).message
        }))        
    }

})