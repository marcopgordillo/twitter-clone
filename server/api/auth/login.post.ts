import { defineEventHandler, readBody, sendError, createError } from 'h3'
import bcrypt from 'bcrypt'
import { createRefreshToken } from "~/server/db/refreshTokens"
import { getUserByUsername } from '~/server/db/users'
import { generateTokens, sendRefreshToken } from '~/server/utils/jwt'
import { userTransformer } from '~/server/transformers/user';

export default defineEventHandler(async (event: any) => {
    const { username, password } = await readBody(event)

    if (!username || !password) {
        return sendError(event, createError({
            statusCode: 419,
            statusMessage: 'Invalid Params'
        }))
    }

    // Is the user registered?
    const user = await getUserByUsername(username)

    if (!user) {
        return sendError(event, createError({
            statusCode: 419,
            statusMessage: 'Username or password is invalid'
        }))
    }

    // Compare passwords
    const doesThePasswordMatch = await bcrypt.compare(password, user.password)

    if (!doesThePasswordMatch) {
        return sendError(event, createError({
            statusCode: 419,
            statusMessage: 'Username or password is invalid'
        }))
    }

    // Generate Access token and Refresh token
    const { accessToken, refreshToken } = generateTokens(user)
    
    // Save inside db
    await createRefreshToken({
        token: refreshToken,
        userId: user.id,
    })

    // add http only cookie
    sendRefreshToken(event, refreshToken)

    return {
        access_token: accessToken,
        user: userTransformer(user),
    }
})