import { defineEventHandler, sendError, createError } from 'h3'
import { getRefreshTokenByToken } from '~/server/db/refreshTokens';
import { getUserById } from '~/server/db/users';
import { decodeRefreshToken, generateTokens } from '~/server/utils/jwt';

export default defineEventHandler(async (event: any) => {

    try {
        // Get the cookie refresh_token
        const cookies = useCookies(event)
        const refreshToken = (cookies as any).refresh_token

        // Verify if it is in the database
        if (!refreshToken) throw Error('Refresh Token is undefined')
        const rToken = await getDecodedToken(refreshToken)
        if (!rToken) Error("Refresh Token doesn't exist in db")

        // Get the access_token
        const accessToken = await getAccessToken(refreshToken)

        return { access_token: accessToken }
    } catch (error) {
        return sendError(event, createError({
            statusCode: 401,
            statusMessage: 'Refresh token is invalid',
        }))
        
    }


    // try {
    //     const user = await getUserById((token as any).userId)
    //     const { accessToken } = generateTokens(user)
    //     return { access_token: accessToken }
    // } catch (error) {
    //     return sendError(event, createError({
    //         statusCode: 500,
    //         statusMessage: 'Something went wrong',
    //     }))
    // }
})

const getAccessToken = async (refreshToken: string) => {
    const token = decodeRefreshToken(refreshToken)
    try {
        const user = await getUserById((token as any).userId)
        const { accessToken } = generateTokens(user)
        return accessToken
    } catch (error) {
        throw error
    }
}

const getDecodedToken = async (refreshToken: string) => {
    const rToken = await getRefreshTokenByToken(refreshToken)
    return rToken
}
