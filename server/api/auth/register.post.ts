import { defineEventHandler, readBody, sendError, createError } from 'h3'
import { createUser } from '~/server/db/users';
import { userTransformer } from '~/server/transformers/user'

export default defineEventHandler(async (event: any) => {
    const body = await readBody(event)

    const { username, email, password, passwordConfirmation, name } = body

    console.log(username, email, password, passwordConfirmation, name)

    if (!username || !email || !password || !passwordConfirmation || !name) {
        return sendError(event, createError({ statusCode: 419, statusMessage: 'Invalid Params' }))
    }

    if (password !== passwordConfirmation) {
        return sendError(event, createError({ statusCode: 419, statusMessage: 'Password do not match' }))
    }

    const userData = {
        username,
        email,
        password,
        name,
        profileImage: 'https://picsum.photos/200/200'
    }

    const user = await createUser(userData)

    return {
        body: userTransformer(user),
    }
})