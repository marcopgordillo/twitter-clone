import { defineEventHandler } from 'h3'
import formidable from 'formidable'
import { createTweet } from '~/server/db/tweets'
import { createMediaFile } from '~/server/db/mediaFiles'
import { tweetTransformer } from '~/server/transformers/tweet'
import { uploadToCloudinary } from '~/server/utils/cloudinary'

export default defineEventHandler(async (event) => {
    const form = formidable({})

    const response = await new Promise((resolve, reject) => {
        form.parse(event.req, (err, fields, files) => {
            if(err) reject(err)

            resolve({ fields, files })
        })
    })


    const { fields, files } = response as any

    const userId = event.context?.auth.user.id

    const tweetData: any = {
        authorId: userId,
        text: fields.text,
        replyToId: (fields.replyTo && fields.replyTo !== undefined) ? fields.replyId : null
    }

    // const replyTo = fields.replyTo

    // if (replyTo && replyTo !== null && replyTo !== undefined) {
    //     tweetData.replyToId = replyTo
    //     console.log('replyTo: ', replyTo)
    // }

    const tweet = await createTweet(tweetData)

    const filePromises = Object.keys(files).map(async (key) => {
        const file = files[key]

        const cloudinaryResource: any = await uploadToCloudinary(file.filepath)

        return createMediaFile({
            url: cloudinaryResource.secure_url,
            providerPublicId: cloudinaryResource.public_id,
            userId: userId,
            tweetId: tweet.id 
        })
    })

    await Promise.all(filePromises)

    return {
        tweet: tweetTransformer(tweet)
    }

})
