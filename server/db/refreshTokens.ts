import { prisma } from '.'

const createRefreshToken = (refreshToken: any) => {
    return prisma.refreshToken.create({
        data: refreshToken
    })
}

const getRefreshTokenByToken = (token: string) => {
    return prisma.refreshToken.findUnique({
        where: {
            token
        }
    })
}

const removeRefreshToken = (token: string) => {
    return prisma.refreshToken.delete({
        where: {
            token: token,
        }
    })
}

export {
    createRefreshToken,
    getRefreshTokenByToken,
    removeRefreshToken,
}