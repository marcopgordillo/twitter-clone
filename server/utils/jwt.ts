import jwt from 'jsonwebtoken'

const config = useRuntimeConfig()

const generateAccessToken = (user) => {
    const config = useRuntimeConfig()

    return jwt.sign({ userId: user.id }, config.jwtAccessToken, {
        expiresIn: '10m'
    })
}

const generateRefreshToken = (user) => {
    return jwt.sign({ userId: user.id }, config.jwtRefreshToken, {
        expiresIn: '4h'
    })

}

const decodeAccessToken = (token: string) => {
    try {
        return jwt.verify(token, config.jwtAccessToken)
    } catch (error) {
        return null 
    }
}

const decodeRefreshToken = (token: string) => {
    try {
        return jwt.verify(token, config.jwtRefreshToken)
    } catch (error) {
        return null 
    }
}

const generateTokens = (user) => {
    const accessToken = generateAccessToken(user)    
    const refreshToken = generateRefreshToken(user)
    
    return {
        accessToken,
        refreshToken,
    }
}

const sendRefreshToken = (event, token) => {
    setCookie(event.res, "refresh_token", token, {
        httpOnly: true,
        sameSite: true,
    })
}

export {
    generateTokens,
    sendRefreshToken,
    decodeRefreshToken,
    decodeAccessToken,
}