import jwtDecode from 'jwt-decode'

export default () => {
    const useAuthToken = () => useState('auth_token')
    const useAuthUser = () => useState('auth_user')
    const useAuthLoading = () => useState('auth_loading', () => false)

    const setToken = (newToken: string) => {
        const authToken = useAuthToken()
        authToken.value = newToken
    }

    const setUser = (newUser: any) => {
        const authUser = useAuthUser()
        authUser.value = newUser
    }

    const setLoading = (loading: boolean) => {
        const authLoading = useAuthLoading()
        authLoading.value = loading
    }

    const login = ({username, password}) => {
        return new Promise(async (resolve, reject) => {
            try {
                const { data } = await useFetch('/api/auth/login', {
                    method: 'POST',
                    body: {
                        username,
                        password
                    },
                })

                setToken(data.value.access_token)
                setUser(data.value.user)
                resolve(true)
            } catch(error) {
                console.log(error)
                reject(error)
            }
        })
    }

    const logout = () => {
        return new Promise(async (resolve, reject) => {
            try {
                await useFetchApi('/api/auth/logout', {
                    method: 'POST',
                })

                setToken(null)
                setUser(null)
                resolve(true)
            } catch (error) {
                reject(error)
            }
        })
    }

    const refreshToken = () => {
        return new Promise(async (resolve, reject) => {
            try {
                const data = await $fetch('/api/auth/refresh')
                if (!(data as any)?.access_token) {
                    throw Error('Unauthenticated')
                }
                setToken((data as any).access_token)
                resolve(true)
            } catch (error) {
                reject(error)
            }
        })
    }

    const getUser = () => {
        return new Promise(async (resolve, reject) => {
            try {
                const data = await useFetchApi('/api/auth/user')
                if (!(data as any)?.user) {
                    throw Error('Unauthenticated')
                }
                setUser((data as any).user)
                resolve(true)
            } catch (error) {
                reject(error)
            }
        })
    }

    const reRefreshAccessToken = () => {
        const authToken = useAuthToken()

        if (!authToken.value) return

        const jwt = jwtDecode(authToken.value as string)

        const newRefreshTime = (jwt as any).exp - 60000

        setTimeout(async () => {
            await refreshToken()
            reRefreshAccessToken()
        }, newRefreshTime)
    }

    const initAuth = () => {
        return new Promise(async (resolve, reject) => {
            setLoading(true)
            try {
                await refreshToken()
                await getUser()
                reRefreshAccessToken()

                resolve(true)
            } catch (error) {
                reject(error)
            } finally {
                setLoading(false)
            }
        })
    }

    return { 
        login,
        useAuthUser,
        useAuthToken,
        useAuthLoading,
        initAuth,
        logout,
    }
}