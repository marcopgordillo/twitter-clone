import { Config } from 'tailwindcss'
import plugin from 'tailwindcss/plugin'
import defaultTheme from 'tailwindcss/defaultTheme'
import tailwindcssForms from '@tailwindcss/forms'

export default <Config> {
  darkMode: 'class',
  safelist: [
    'text-sm',
    {
      pattern: /(text|bg)-(red|green|yellow|blue)-(100|400)/,
      variants: ['group-hover']
    },
  ],
  content: [],
  theme: {
    extend: {
      colors: {
        dim: {
          50: "#5F99F7",
          100: "#5F99F7",
          200: "#38444d",
          300: "#202e3a",
          400: "#253341",
          500: "#5F99F7",
          600: "#5F99F7",
          700: "#192734",
          800: "#162d40",
          900: "#15202b",
        },
      },
    }
  },
  plugins: [
    tailwindcssForms,
    plugin(({ addUtilities, theme }) => {
      addUtilities({
        '.transition-default': {
          '@apply transition ease-in-out duration-300': {},
        },
        '.content-auto': {
          'content-visibility': 'auto',
        },
        '.content-hidden': {
          'content-visibility': 'hidden',
        },
        '.content-visible': {
          'content-visibility': 'visible',
        },
        '.border-twitter': {
          '@apply border-gray-200 dark:border-gray-700': {}
        },
      })
    }),
  ],
}