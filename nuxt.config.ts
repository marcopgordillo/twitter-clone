import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
    typescript: {
        shim: false,
    },
    modules: [
        '@nuxtjs/color-mode',
        '@nuxtjs/tailwindcss',
    ],
    build: {
        transpile: ['@heroicons/vue']
    },
    colorMode: {
        preference: 'system',
        classPrefix: '',
        classSuffix: '',
        dataValue: 'theme',
    },

    runtimeConfig: {
        jwtAccessToken: process.env.JWT_ACCESS_TOKEN_SECRET,
        jwtRefreshToken: process.env.JWT_REFRESH_TOKEN_SECRET,
        cloudinaryCloudName: process.env.CLOUDINARY_CLOUD_NAME,
        cloudinaryApiKey: process.env.CLOUDINARY_API_KEY,
        cloudinaryApiSecret: process.env.CLOUDINARY_API_SECRET,
    }
})
